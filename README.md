# Project 27-Media-Player

Media player sa podrskom za vise fajlova, kao i mogucnost anotacije videa

## Developers

- [Mladen Dilparić, 201/2015](https://gitlab.com/giomla93)
- [Sara Kapetinić, 182/2017](https://gitlab.com/SaraKapetinic_mi17182)
- [Filip Nedeljković, 305/2017](https://gitlab.com/efen16)
- [Nikola Panić, 284/2017](https://gitlab.com/mrpannic)
- [Nikola Nikolić, 35/2015](https://gitlab.com/nnikolic1)
